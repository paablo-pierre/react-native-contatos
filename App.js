import { StackNavigator } from 'react-navigation';

import PeoplePage from './src/pages/PeoplePage';
import PeopleDetailPage from './src/pages/PeopleDetailPage';
import capitalizeFirstLetter from './src/util/capitalizeFirstLetter';

export default StackNavigator ({
  'Main': {
    screen: PeoplePage
	},
	'PeopleDetail': {
		screen: PeopleDetailPage,
		navigationOptions: ({ navigation }) => {
			const peopleFirstName = capitalizeFirstLetter(navigation.state.params.people.name.first);
			const peopleLastName = capitalizeFirstLetter (navigation.state.params.people.name.last);
			const name = peopleFirstName + ' ' + peopleLastName;
			return ({
				title: name,
				headerTitleStyle: {
					color: 'white',
					fontSize: 30,
					textAlign: 'center'
				}
			});
		}
	}
}, {
	navigationOptions: {
		title: 'Pessoas!',
		headerTintColor: 'white',
		headerStyle: {
			backgroundColor: '#6ca2f7',
			borderBottomWidth: 1,
			borderBottomColor: '#C5C5C5'
		},
		headerTitleStyle: {
			color: 'white',
			fontSize: 30,
			flex: 1,
			textAlign: 'center'
		}
	}
});