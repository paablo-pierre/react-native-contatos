import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Alert } from 'react-native';

import { capitalazeFirstLetter } from '../util';

export default class PeopleListItem extends Component {

    render() {

        const { people, navigateToPeopleDetail } = this.props;
        const { title, first, last } = people.name;
        return(
            <TouchableOpacity onPress={() => navigateToPeopleDetail({ people })} >
                <View style={styles.line}>
                    <Image 
                        source={{
                            uri: people.picture.thumbnail
                        }}
                        style={styles.avatar} 
                    />
                    <Text style={styles.lineText}> 
                        { `${
                                capitalazeFirstLetter(title)
                        } ${
                                capitalazeFirstLetter(first)
                        } ${    
                                capitalazeFirstLetter(last)
                            }`
                        } 
                    
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({
    line: {
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: '#bbb',
        alignItems: 'center',
        flexDirection: 'row',
    },
    lineText: {
        fontSize: 20,
        paddingLeft: 20,
        flex: 7
    },
    avatar: {
        aspectRatio: 1,
        flex: 1,
        marginLeft: 15,
        borderRadius: 50
    }
});