import React, { Component } from 'react';
import { View, Text } from 'react-native';

export default class Header extends Component {
    render() {
        return (
            <View style={containerStyle.container}>
                <Text style={containerStyle.titleStyle}> {this.props.title} </Text>
            </View>
        );
    }
}

const containerStyle = {
    container: { 
        marginTop: 0,
        backgroundColor: '#6ca2f7',
        alignItems: 'center',
        justifyContent: 'center'    
    },
    titleStyle: {
        fontSize: 50,
        color: '#fff'
    }
        
};