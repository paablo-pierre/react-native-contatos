import React, { Component } from 'react';
import { FlatList, Text, StyleSheet } from 'react-native';

import PeopleListItem from './PeopleListItem';

/*
  *Lista de contatos sendo efetuada por props
*/
export default class PeopleList extends Component {
    /*
      * chamando o array de elementos em items
      * que é renderizado em PeopleListItem
    */
    render() {
        const { peoples, onPressItem } = this.props;

        const items = peoples.map (people => {
            return ( <PeopleListItem 
                key = {people.name.first} 
                people={people}
                navigateToPeopleDetail={onPressItem} />
            );
        });

        return(

            <FlatList 
                style={styles.container}
                data={peoples}
                renderItem={({ item }) => (
                    <PeopleListItem  
                    people={item}
                navigateToPeopleDetail={onPressItem}/>)}
                keyExtractor={item => item.name.first}
            />
        );
    }
}

const styles = StyleSheet.create ({
    container: {
        backgroundColor: '#e2f9ff',
    },
});