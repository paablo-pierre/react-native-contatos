import React, { Component } from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';

import PeopleList from '../components/PeopleList';

import axios from 'axios';

export default class PeoplePage extends Component {
  constructor(props) {
    super(props);

    this.state= {
      peoples: [],
      loading: false,
      error: false,
    }
  }

/*
  * chamada do component usando a api random user 
  * para receber uma lista com nomes de pessoas
*/

  componentDidMount() {
    this.setState({ loading: true })
    axios.get('https://randomuser.me/api/?nat=br&results=20')
      .then(response => {
        const { results } = response.data;
        this.setState ({
          peoples: results,
          loading: false
        });
      }).catch(error => {
        this.setState({error: true, loading: false})
      });
  }

  renderPage() {
    if (this.state.loading) {
      return <ActivityIndicator size="large" color="#6ca2f7" /> 
    } 
    if (this.state.error) {
      return<Text style={styles.error} >Ops... Algo deu errado :/</Text>
    } else {
      return <PeopleList peoples= 
                {this.state.peoples} 
                onPressItem={ pageParams => 
                  {this.props.navigation.navigate('PeopleDetail', pageParams)
              }}/>
    }
  }

  render() {
    return (
      <View style={styles.container} > 
        { this.renderPage() }
      </View>
    );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  error: {
    color: "red",
    alignSelf: 'center',
    fontSize: 20
  }
});